#include <Esp.h>
#include <SPI.h>

#include "happycat_oled_64.xbm"

#define LCD_CS 15
#define LCD_DC 5
#define LCD_RESET 4

#define MARK_CS digitalWrite(LCD_CS, LOW)
#define RELEASE_CS digitalWrite(LCD_CS, HIGH)

#define MARK_RESET digitalWrite(LCD_RESET, LOW)
#define RELEASE_RESET digitalWrite(LCD_RESET, HIGH)

#define LCD_DATA digitalWrite(LCD_DC, HIGH)
#define LCD_CMD digitalWrite(LCD_DC, LOW)

void sendCommand(uint8_t cmd) {
    LCD_CMD;
    MARK_CS;
    SPI.write(cmd);
    RELEASE_CS;
}

void setup() {
    pinMode(16, OUTPUT);

    RELEASE_RESET;
    pinMode(LCD_RESET, OUTPUT); // RESET
    // MARK_RESET;
    
    RELEASE_CS;
    pinMode(LCD_CS, OUTPUT);

    LCD_DATA;
    pinMode(LCD_DC, OUTPUT); // #C/D

    SPI.begin();

    MARK_RESET;
    delay(10);
    RELEASE_RESET;
    delay(40);

    // ST7565
    sendCommand(0xE2); delay(50); // Reset

    sendCommand(0xAE); delay(1); // Display OFF
    sendCommand(0xA2); // delay(1); // Sets the LCD drive voltage bias ratio 0: 1/9 bias

    // sendCommand(0xA0); delay(1); // Segment direction normal
    // sendCommand(0xC0); delay(1); // Common direction normal
    sendCommand(0xA1); // delay(1); // Segment direction reverse
    sendCommand(0xC8); // delay(1); // Common direction reverse

    sendCommand(0xA4); // delay(1); // Display all points OFF
    sendCommand(0x40 | (0 & 0x3F)); // Display start line
    sendCommand(0x26); // delay(1); // V5 voltage regulator
    sendCommand(0x81); sendCommand(0x07); // Contrast (0-63). Original 0x0F , hingly not recommended.
    // sendCommand(0xF8); sendCommand(0x01); // Test sequence?
    sendCommand(0x2F); delay(1); // Power controller (all on)
    sendCommand(0xAF); delay(10); // delay(100); // Display ON

    // sendCommand(0xA7); // Display inverse
    // sendCommand(0xA6); // Display normal
}

int fill = 0x00;

void loop() {
    digitalWrite(16, HIGH);
    uint8_t *p_fb = happycat_oled_64_bits;
    for(int y=0; y<8; y++) {
        sendCommand(0xB0 | (y & 0x0F)); // Page set address
        sendCommand(0x10 | (0 & 0x0F)); // Column set address. high bits
        sendCommand(0x00 | (4 & 0x0F)); // Column set address. low bits
        LCD_DATA;
        MARK_CS;
        for(int x = 0; x<128; x++) {
            SPI.write(happycat_oled_64_bits[x*8+y]);
        }
        RELEASE_CS;
    }
    digitalWrite(16, LOW);

    delay(1000);
}
